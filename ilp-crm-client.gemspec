# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ilp_crm_client/version'

Gem::Specification.new do |spec|
  spec.name          = 'ilp_crm_client'
  spec.version       = IlpCrmClient::VERSION
  spec.authors       = ['Igor Kutyavin', 'Oleg Pastushenko']
  spec.email         = []
  spec.description   = ''
  spec.summary       = ''
  spec.homepage      = ''
  spec.license       = ''

  spec.files         = Dir['{lib}/**/*']
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake'

  spec.add_runtime_dependency 'faraday', '~> 0.9'
  spec.add_runtime_dependency 'httpclient'
  spec.add_runtime_dependency 'mash'
end