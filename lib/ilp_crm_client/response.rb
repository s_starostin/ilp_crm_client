module IlpCrmClient
  class Response
    RESULT_SUCCESS = '1'

    attr_accessor :body, :hash

    def initialize(body)
      @body = body
      @hash = Mash.new Hash.from_xml(body)
    end

    def response
      hash.response
    end

    def success?
      response.result == RESULT_SUCCESS
    end

    def bad_fields?
      response.key? :bad_fields
    end

    def bad_fields
      response.bad_fields
    end

    def resultmsg?
      response.key? :resultmsg
    end

    def resultmsg
      response.resultmsg
    end

    def result
      response.result
    end
  end
end