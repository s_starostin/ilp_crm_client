require 'faraday'
require 'mash'
require 'ilp_crm_client/configuration'
require 'ilp_crm_client/response'

module IlpCrmClient
  TIMEOUT = 180

  class << self
    attr_accessor :configuration

    def configure
      self.configuration ||= Configuration.new
      yield(configuration)
    end

    def call(method, params = nil)
      hash = { action: method.to_s.camelize, secretcode: configuration.secret_code }
      hash.merge!({ params: params }) if params.present?
      response = client.post do |req|
        req.body = hash.to_xml(root: :request, dasherize: false)

        logger.info "=============== ILP CRM REQUEST START ==============="
        logger.info req.body
        logger.info "=============== ILP CRM REQUEST END ==============="

      end
      
      logger.info "=============== ILP CRM RESPONSE START ==============="
      logger.info response.env.body
      logger.info "=============== ILP CRM RESPONSE END ==============="

      Response.new response.env.body
    end

    def client
      @client ||= Faraday.new(configuration.url, ssl: { verify: false }) do |faraday|
        faraday.response :logger
        faraday.adapter :net_http
        faraday.options.timeout = TIMEOUT
        faraday.headers['Content-Type'] = 'application/xml'
        faraday.basic_auth configuration.user, configuration.password
      end
    end

    def logger
      @logger ||= Logger.new("#{Rails.root}/log/ilp.log")
    end
  end
end